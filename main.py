# -*- coding: utf-8 -*-

__author__ = 'bobby'

from scrapy.cmdline import execute

import sys
import os

# 获取当前项目的路径
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
# 在这里运行爬虫和在命令行运行爬虫是一样的，只不过把命令行运行爬虫的命令放到了这里，然后上面是获取项目的路径
execute(["scrapy","crawl","articles"])
