# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from urllib import parse

class ArticlesSpider(scrapy.Spider):
    name = 'articles'
    allowed_domains = ['www.news.cnblogs.com']
    start_urls = ['https://news.cnblogs.com/n/comment?type=hot&page=1']

    def parse(self, response):
        '''
        工作内容
        1、获取文章列表中的文章url
        2、获取下一页url并交给scrapy进行下载
        :param response:
        :return:
        '''
        ## 获取列表页文章url
        postUrl = response.xpath("//span[contains(@id,'comment_news_title')]/a/@href").extract()
        for url in postUrl:
            # 在这里遍历完成之后，再传递给下面的方法进行解析
            # yield Request(url = parse.urljoin(response.url,url),callback = self.pars_detail)
            yield Request(url = "https://news.cnblogs.com" + url, callback=self.parse_detail)

        # 提取下一页，并交给scrapy进行下载
        next = response.xpath('//*[@id="pages"]//@href').extract_first()
        print(next)
        if next:
            yield Request(url="https://news.cnblogs.com" + next, callback=self.parse)



    def parse_detail(self,response):
        # 提取文章的具体字段
        title = response.xpath("//div[contains(@id,'news_title')]/a/text()").extract_first().strip()
        time = response.xpath("//div[contains(@id,'news_info')]/span[2]/text()").extract_first().replace("发布于 ","")
        print(title)

        pass
